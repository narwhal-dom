2
var rhinodom_package = Packages.com.metaweb.util.javascript.DOM;
// generated with: ls | cut -d. -2f 1 | awk '{ print "\""  $1  "\"" }' | paste -s -d, - | awk '{ print "\[" $1 "\]" }'
var rdom_parts =  ["JSAttr","JSCDATASection","JSCharacterData","JSComment",
                   "JSDOMException","JSDOMImplementation","JSDOMParser",
                   "JSDOMParserException","JSDocument","JSDocumentFragment",
                   "JSDocumentType","JSElement","JSEntity","JSEntityReference",
                   "JSNamedNodeMap","JSNode","JSNodeList","JSNotation",
                   "JSProcessingInstruction","JSText"];

var dom_scope = {};
var dclass = Packages.org.mozilla.javascript.ScriptableObject.defineClass;
for (var i = 0; i < rdom_parts.length; i++) {
    var part = rdom_parts[i];
    dclass(dom_scope, rhinodom_package[part], false, true);
}

dom_scope.Attr.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.CDATASection.prototype.constructor.prototype.__proto__ =
    dom_scope.Text.prototype;
dom_scope.CharacterData.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.Comment.prototype.constructor.prototype.__proto__ =
    dom_scope.CharacterData.prototype;
dom_scope.Document.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.DocumentFragment.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.DocumentType.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.Element.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.Entity.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.EntityReference.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.Notation.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.ProcessingInstruction.prototype.constructor.prototype.__proto__ =
    dom_scope.Node.prototype;
dom_scope.Text.prototype.constructor.prototype.__proto__ =
    dom_scope.CharacterData.prototype;

exports.DOMParser = new dom_scope.DOMParser(dom_scope);
exports.dom_objects = dom_scope;
